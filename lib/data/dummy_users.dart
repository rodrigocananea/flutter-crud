import 'package:flutter_crud/models/user.dart';

const DUMMY_USERS = {
  '1': const User(
      id: '1',
      name: 'Maria',
      email: "mara@aluna.com.br",
      avatarUrl:
          'https://cdn.pixabay.com/photo/2016/03/31/19/58/avatar-1295429_960_720.png'),
  '2': const User(
      id: '2', name: 'Joao', email: "Joao@aluna.com.br", avatarUrl: null),
};
